﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainsawRotation : MonoBehaviour
{
    //All the stuff commented out here is supposed to make the chainsaw flip around and move to the other shoulder when the mouse is aimed behind Blastov
    //However since target is declared elsewhere and I couldn't get it to work here, I'll leave this until later - Brendan
    public Transform target;
    public float rotation;
    public Transform mouse;
    public GameObject mousePos;
    public SpriteRenderer gunSprite;
    protected Vector3 direction;
    private float xPos, yPos, zPos;

    // Start is called before the first frame update
    void Start()
    {
        target = mousePos.transform;
        //gunSprite = gameObject.GetComponent<SpriteRenderer>();
        //xPos = gunSprite.transform.localPosition.x;
        //yPos = gunSprite.transform.localPosition.y;
        //zPos = gunSprite.transform.localPosition.z;
    }

    // Update is called once per frame
    void Update()
    {
        Rotate(mouse.transform.position - transform.position);
    }



    void Rotate(Vector3 rot)
    {
        Vector3 dir =  rot;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle , Vector3.forward);

        // direction = target.position - transform.position;
        // direction = direction.normalized;
        // if (direction.x < 0)
        // {
        //     gunSprite.flipY = true;
        //     gunSprite.transform.localPosition = new Vector3(xPos * -1, yPos, zPos);
        // }
        // else if (direction.x >= 0)
        // {
        //     gunSprite.flipY = false;
        //     gunSprite.transform.localPosition = new Vector3(xPos, yPos, zPos);
        // }
    }
}

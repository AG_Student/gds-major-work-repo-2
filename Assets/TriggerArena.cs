﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerArena : MonoBehaviour
{
    public GameObject arena;



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            arena.SetActive(true);
            Destroy(this.gameObject);
        }

    }
}

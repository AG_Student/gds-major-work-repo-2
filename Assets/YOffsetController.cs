﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class YOffsetController : MonoBehaviour
{
    public float maxYOffset = 5, sensitivity = 1;
    CinemachineOrbitalTransposer vcam;
    float y;


    // Start is called before the first frame update
    void Start()
    {
        vcam = GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineOrbitalTransposer>();
    }

    // Update is called once per frame
    void Update()
    {
        y += Input.GetAxis("Mouse Y") * sensitivity;
        y = Mathf.Clamp(y, -maxYOffset, maxYOffset);
    }

    private void FixedUpdate()
    {
        vcam.m_FollowOffset.y = y;
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullHook : GrappleHook
{
    // Start is called before the first frame update

    public float pullForce;
    public bool pulling = false;

    new void Start()
    {
        base.Start();
    }

    private void FixedUpdate()
    {
        if (attached)
            transform.position = transform.parent.transform.position;
    }

    public new void OnCollisionEnter2D(Collision2D collision)
    {
        if (targetLayers.Contains(collision.gameObject.layer) || collision.gameObject.tag == "Pickup")
        {
            Attach(collision.gameObject);
        }
    }

    public void Attach(GameObject target)
    {
        if (target.layer == LayerMask.NameToLayer("Enemy") || target.tag == "Pickup")
        {
            Debug.Log(1);
            rb.velocity = Vector2.zero;
            gameObject.transform.parent = target.gameObject.transform;
            attached = true;
            gameObject.layer = 0;
            StartCoroutine(ApplyPullForce(target));
        }
        else
        {
            Debug.Log(2);
            Unattach();
        }
    }

    public IEnumerator ApplyPullForce(GameObject target)
    {
        if(target.tag != "Pickup")
            target.GetComponent<I_Enemy>().Stun();
        pulling = true;
        while (pulling)
        {
            Vector2 dir = player.transform.position - target.transform.position;
            dir = dir.normalized;
            //target.GetComponent<Rigidbody2D>().AddForce(dir * pullForce, ForceMode2D.Impulse);
            //pulling = false;
            target.GetComponent<Rigidbody2D>().velocity = dir * pullForce;
            yield return null;
        }
        Unattach();
    }

    public new void Unattach()
    {
        attached = false; 
        gun.firing = false;
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            pulling = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            pulling = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    // example audioclip
    //public AudioClip x;

    [Header("Voice Lines")]
    public AudioClip voiceLineOne;
    public AudioClip voiceLineTwo;
    public AudioClip voiceLineThree;
    public AudioClip voiceLineFour;
    public AudioClip voiceLineFive;
    public AudioClip voiceLineSix;
    public AudioClip voiceLineSeven;
    public AudioClip voiceLineEight;
    public AudioClip voiceLineNine;
    public AudioClip levelComplete;

    public AudioSource _audioSource;
    private int j;

    [Header("Sound Effects")]
    public AudioSource[] effectChannels;

    public AudioClip burning;
    public AudioClip hit;
    public AudioClip pickup;
    public AudioClip lowHP;
    public AudioClip enemyExplosion;

    [Header("Weapon Effects")]
    public AudioClip fireRocket;
    public AudioClip explosion;
    public AudioClip gunShot;
    public AudioClip chainsaw;
    public AudioClip chainsawIdle;
    public AudioSource weaponChannel1;
    public AudioSource weaponChannel2;
    public AudioSource chainsawChannel;

    private static AudioManager instance;
    public static AudioManager Instance
    {
        get
        {
            return instance;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        _audioSource = gameObject.GetComponent<AudioSource>();
        j = 0;
    }


    // example method
    // public void Playx()
    //{
    //    _audioSource.PlayOneShot(x);

    //}

    //activate random voiceLine
    public void randomVoiceLine()
    {
        Debug.Log("sound");
        int i = Random.Range(1, 10);
        if (j == i)
        {
            randomVoiceLine();
        }
        else
        {
            j = i;
            Debug.Log(i);
            if (i == 1)
            {
                _audioSource.PlayOneShot(voiceLineOne);
            }
            if (i == 2)
            {
                _audioSource.PlayOneShot(voiceLineTwo);
            }
            if (i == 3)
            {
                _audioSource.PlayOneShot(voiceLineThree);
            }
            if (i == 4)
            {
                _audioSource.PlayOneShot(voiceLineFour);
            }
            if (i == 5)
            {
                _audioSource.PlayOneShot(voiceLineFive);
            }
            if (i == 6)
            {
                _audioSource.PlayOneShot(voiceLineSix);
            }
            if (i == 7)
            {
                _audioSource.PlayOneShot(voiceLineSeven);
            }
            if (i == 8)
            {
                _audioSource.PlayOneShot(voiceLineEight);
            }
            if (i == 9)
            {
                _audioSource.PlayOneShot(voiceLineNine);
            }
        }
    }

    //end level voiceline
    public void levelEnd()
    {
        _audioSource.PlayOneShot(levelComplete);
    }

    //
    //  Sound Effects
    //

    private AudioSource PickEffectChannel()
    {
        foreach(AudioSource channel in effectChannels)
        {
            if (!channel.isPlaying)
            {
                return channel;
            }
        }

        //If it gets to this point then there are no free channels!
        return null;
    }

    public void PlayBurning()
    {
        PickEffectChannel().PlayOneShot(burning, 0.5f);
    }

    public void PlayHit()
    {
        PickEffectChannel().PlayOneShot(hit, 0.5f);
    }

    public void PlayPickup()
    {
        PickEffectChannel().PlayOneShot(pickup, 0.5f);
    }

    public void PlayLowHP()
    {
        PickEffectChannel().PlayOneShot(lowHP, 0.5f);
    }

    public void PlayEnemyExplosion()
    {
        PickEffectChannel().PlayOneShot(enemyExplosion, 1f);
    }

    public void PlayFireRocket()
    {
        weaponChannel2.PlayOneShot(fireRocket, 0.5f);
    }

    public void PlayExplosion()
    {
        weaponChannel2.PlayOneShot(explosion, 0.5f);
    }

    public void PlayGunShot()
    {
        weaponChannel1.PlayOneShot(gunShot, 0.5f);
    }

    public void SetChainsawClip(int mode)
    {
        if(mode == 0)
        {
            chainsawChannel.clip = chainsaw;
        }
        else if(mode == 1)
        {
            chainsawChannel.clip = chainsawIdle;
        }
    }

    public void PlayChainsaw(bool play)
    {
        if (play)
        {
            if (!chainsawChannel.isPlaying)
            {
                chainsawChannel.Play();
            }
            
        }
        else
        {
            chainsawChannel.Stop();
        }
    }
}

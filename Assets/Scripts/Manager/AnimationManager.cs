﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    public Animator machineGunAnimator;
    public Animator playerAnimator;

    private static AnimationManager instance;
    public static AnimationManager Instance
    {
        get
        {
            return instance;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }


    public void MGIsFiringTrue()
    {
        machineGunAnimator.SetBool("isFiring", true);
    }

    public void MGIsFiringFalse()
    {
        machineGunAnimator.SetBool("isFiring", false);
    }

    public void isGroundedTrue(){
      playerAnimator.SetBool("isGrounded", true);
    }

    public void isGroundedFalse(){
      playerAnimator.SetBool("isGrounded", false);
    }

    public void setSpeed(float speed)
    {
        playerAnimator.SetFloat("isRunning", Mathf.Abs(speed));
    }


}

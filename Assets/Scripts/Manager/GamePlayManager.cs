﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Experimental.Rendering.Universal;

public class GamePlayManager : MonoBehaviour
{
    public int[] oddsOn;
    public float maxHealth = 3;
    public int bounceHeight;

    public float halfHealth;
    public float quaterHealth;

    public GameObject healthBar;
    public float currentPlayerHealth;

    public List<Light2D> lights;
    public Color tempColor;

    private static GamePlayManager instance;
    public static GamePlayManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Start()
    {
        instance = this;

        CalculateHealth();

    }


    public void SetLightsRed()
    {
        
        foreach(Light2D light in lights)
        {
            light.color = Color.red;
        }
    }

    public void SetLightsYellow()
    {
         
            foreach(Light2D light in lights)
            {
                light.color = tempColor;
            }
         
    }

    void CalculateHealth()
    {
        halfHealth = Mathf.Round((maxHealth + 0.5f) / 2);
        quaterHealth = Mathf.Round((maxHealth + 0.5f) / 4);
    }

    public void ReloadScene()
    {
        string thisScene = SceneManager.GetActiveScene().name;
        Debug.Log(thisScene);
        SceneManager.LoadScene(thisScene);
    }
}

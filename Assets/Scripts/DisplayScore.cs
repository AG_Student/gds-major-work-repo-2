﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour
{
    private static DisplayScore instance;
    public static DisplayScore Instance
    {
        get
        {
            return instance;
        }
    }

    public Text Score;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public void displayPoints(int points, Vector3 pos)
    {
        Debug.Log(points);
        //Vector3 pos = this.transform.position;
        Vector2 viewPortPoint = Camera.main.WorldToScreenPoint(pos);
        Debug.Log(pos);
        Score.GetComponent<RectTransform>().position = viewPortPoint;
        Score.text = "" + points;
        Score.GetComponent<Text>().enabled = true;
        StartCoroutine(disable(1f));
    }

    IEnumerator disable (float delay)
    {
        yield return new WaitForSeconds(delay);
        Score.GetComponent<Text>().enabled = false;
    }
}

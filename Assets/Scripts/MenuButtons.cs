﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void exitBtn() {
        Application.Quit();
    }

    public void level1() {
        ApplicationData.currentLevel = ApplicationData.CurrentLevel.level1;
        ApplicationData.spawnLocation = ApplicationData.originalSpawn;
        SceneManager.LoadScene("SampleScene");
    }

    public void level2()
    {
        ApplicationData.currentLevel = ApplicationData.CurrentLevel.level2;
        ApplicationData.spawnLocation = ApplicationData.originalSpawn;
        SceneManager.LoadScene("Level2");

    }
    public void Endless()
    {
        ApplicationData.currentLevel = ApplicationData.CurrentLevel.levelEndless;
        ApplicationData.spawnLocation = ApplicationData.originalSpawn;
        SceneManager.LoadScene("SampleScene 1");
    }
    }

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGun : MonoBehaviour, I_Weapon
{
    public GameObject shot, light, lightHolder;
    public Transform shotOrigin, mouse;
    public PlayerPhysicsController player;
    public int maxAmmo, currentAmmo;
    public WeaponUI ui;
    public float shotTimer = 1, force;
    public bool canShoot = true;
    float shotTimerMax;
    private Renderer spriteRenderer;
    private Renderer leftArmRenderer;
    private Renderer rightArmRenderer;


    // Start is called before the first frame update
    void Start()
    {
        shotTimerMax = shotTimer;
        currentAmmo = maxAmmo;
        mouse = GameObject.FindWithTag("Mouse").transform;
        PlayerController.landEvent += RefillAmmo;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        leftArmRenderer = gameObject.transform.Find("Left_arm").GetComponent<SpriteRenderer>();
        rightArmRenderer = gameObject.transform.parent.Find("Right_arm").GetComponent<SpriteRenderer>();

      }
    private void OnDestroy()
    {
        PlayerController.landEvent -= RefillAmmo;
    }
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Mouse0))
        //{
        //    MachineGunAttack();
        //}
        // if (Input.GetMouseButton(1)){
        //     spriteRenderer.enabled = false;
        //     leftArmRenderer.enabled = false;
        //     rightArmRenderer.enabled = true;
        //   }
        //
        // else{
        //     spriteRenderer.enabled = true;
        //     leftArmRenderer.enabled = true;
        //     rightArmRenderer.enabled = false;
        //   }

        if (shotTimer < shotTimerMax)
        {
            shotTimer += Time.deltaTime;
        }
        else if (shotTimer > shotTimerMax)
        {
            shotTimer = shotTimerMax;
        }
        else if (Input.GetMouseButtonUp(0))
            shotTimer = shotTimerMax;

        PlayAnim();
    }

    public void Attack()
    {
        if (currentAmmo > 0 && shotTimer == shotTimerMax)
        {
            shotTimer = 0;
            if (!player.player.OnGround)
            {
                player.rb.velocity = Vector3.zero;
                player.ApplyForce(-(mouse.position - shotOrigin.position).normalized * force);
            }
            ScreenShakeController.Instance.ShakeCamera(.5f);
            MachineGunBullet bullet = Instantiate(shot, shotOrigin.position, Quaternion.identity).GetComponent<MachineGunBullet>();
            Instantiate(light, lightHolder.transform.position, Quaternion.identity);
            bullet.GetComponent<MachineGunBullet>().Rotate(mouse.transform.position - transform.position);
            bullet.SetTrajectory(mouse.position - shotOrigin.position);
            //currentAmmo--;
            ui.UpdateAmmo(currentAmmo, maxAmmo);
        }
    }

    //public void MachineGunAttack()
    //{
    //    if (currentAmmo > 0 && shotTimer == shotTimerMax)
    //    {
    //        shotTimer = 0;
    //        if (!player.player.OnGround)
    //        {
    //            player.rb.velocity = Vector3.zero;
    //            player.ApplyForce(-(mouse.position - shotOrigin.position).normalized * force);
    //        }
    //        ScreenShakeController.Instance.ShakeCamera(.5f);
    //        MachineGunBullet bullet = Instantiate(shot, shotOrigin.position, Quaternion.identity).GetComponent<MachineGunBullet>();
    //        Instantiate(light, lightHolder.transform.position, Quaternion.identity);
    //        bullet.GetComponent<MachineGunBullet>().Rotate(mouse.transform.position - transform.position);
    //        bullet.SetTrajectory(mouse.position - shotOrigin.position);
    //        currentAmmo--;
    //        ui.UpdateAmmo(currentAmmo, maxAmmo);
    //    }
    //}

        void PlayAnim()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && currentAmmo > 0)
            {
                AnimationManager.Instance.MGIsFiringTrue();
            }

            else if (Input.GetKeyUp(KeyCode.Mouse0) || currentAmmo <= 0)
            {
                AnimationManager.Instance.MGIsFiringFalse();
            }
        }

    public int ReturnCurrentAmmo()
    {
        return currentAmmo;
    }

    public int ReturnMaxAmmo()
    {
        return maxAmmo;
    }

    public void RefillAmmo()
        {
            currentAmmo = maxAmmo;
            ui.UpdateAmmo(currentAmmo, maxAmmo);
       }



}

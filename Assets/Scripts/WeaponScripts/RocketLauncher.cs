﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLauncher : MonoBehaviour, I_Weapon
{
    public Transform shotOrigin;
    public GameObject projectile;
    AttackController controller;
    public GameObject mouse;
    public int maxAmmo = 5, currentAmmo;
    public WeaponUI ui;
    public float shotTimer = 1;
    public bool canShoot = true;
    float shotTimerMax;
    GameObject shot;
   // private Renderer spriteRenderer;

    private void OnEnable()
    {
        controller = GetComponentInParent<AttackController>();
        controller.ChangeWeapon(this.gameObject);
    }


    public void Start()
    {
       // spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        mouse = GameObject.FindWithTag("Mouse");
        currentAmmo = maxAmmo;
        PlayerController.landEvent += RefillAmmo;
        shotTimerMax = shotTimer;

    }

    private void OnDestroy()
    {
        PlayerController.landEvent -= RefillAmmo;
    }

    private void Update()
    {
        if (shotTimer < shotTimerMax)
        {
            shotTimer += Time.deltaTime;
        }
        else if (shotTimer > shotTimerMax)
        {
            shotTimer = shotTimerMax;
        }

        if (Input.GetMouseButton(1)){
            //    spriteRenderer.enabled = true;
            //    RocketAttack();
            DetonateShot();
        }
        //else{
        //    spriteRenderer.enabled = false;
        //  }
    }

    public void Attack()
    {
       if (currentAmmo > 0 && shotTimer == shotTimerMax)
       {
           shot = Instantiate(projectile, shotOrigin.position, Quaternion.identity);
          // currentAmmo--;
           ui.UpdateAmmo(currentAmmo, maxAmmo);
           shotTimer = 0;
           shot.GetComponent<MissileBehaviour>().Rotate(mouse.transform.position - transform.position);
        }


    }

    //public void RocketAttack()
    //{
    //    if (currentAmmo > 0 && shotTimer == shotTimerMax)
    //    {
    //        shot = Instantiate(projectile, shotOrigin.position, Quaternion.identity);
    //        currentAmmo--;
    //        ui.UpdateAmmo(currentAmmo, maxAmmo);
    //        shotTimer = 0;
    //        shot.GetComponent<MissileBehaviour>().Rotate(mouse.transform.position - transform.position);
    //    }
    //}

    public void DetonateShot()
    {
        if (shot != null)
            shot.GetComponent<MissileBehaviour>().Detonate();
        shot = null;
    }

    public int ReturnCurrentAmmo()
    {
        return currentAmmo;
    }

    public int ReturnMaxAmmo()
    {
       return maxAmmo;
    }

    public void RefillAmmo()
    {
        currentAmmo = maxAmmo;
        ui.UpdateAmmo(currentAmmo, maxAmmo);
    }


}

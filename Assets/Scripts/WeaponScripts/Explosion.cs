﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{

    CircleCollider2D col;
    public float minForce, forceIncreaseMultiplier, maxRadius, radiusMultiplier;
    public float knockUpForce;
    public float minForceCheck;
    public int damage;
    public float timer, physicsTimer;

    // Start is called before the first frame update
    void Start()
    {
        ScreenShakeController.Instance.ShakeCamera(2);
        col = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        physicsTimer -= Time.deltaTime;

        if(physicsTimer <= 0f)
        {
            col.enabled = false;
        }
        if (transform.localScale.x < maxRadius)
        {
            transform.localScale += (new Vector3(transform.localScale.x * radiusMultiplier, transform.localScale.y  * radiusMultiplier, transform.localScale.z));
            minForce -= Time.deltaTime * forceIncreaseMultiplier;
        }
        else if (timer <= 0f)
            Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 10)
        {
            collision.gameObject.GetComponent<I_Damageable>().Damage(damage);
            Vector2 dir = transform.position - collision.gameObject.transform.position;
            dir = dir.normalized;
            collision.gameObject.GetComponent<I_Enemy>().Knockback(dir * damage);
        }
        else if(collision.gameObject.tag == "Player")
        {
            PlayerPhysicsController player = collision.gameObject.GetComponent<PlayerPhysicsController>();
            if (!player.player.OnGround)
            {
                Vector2 forceDir = (collision.gameObject.transform.position - transform.position);
                forceDir = forceDir.normalized;
                if (forceDir.y < 0)
                {
                    forceDir.y = 0;
                    forceDir.x *= 2;
                    forceDir = forceDir.normalized;
                }

                
                
                player.rb.velocity = new Vector2(player.rb.velocity.x, 0f);
                player.ApplyForce(forceDir * minForce);
                Debug.Log(forceDir.normalized );
            }
            else if (player.player.OnGround)
            {
                //Vector2 forceDir = (collision.gameObject.transform.position - transform.position);
                Vector2 force = new Vector2(0f, knockUpForce);
                player.rb.velocity = new Vector2(player.rb.velocity.x, 0f);
                player.ApplyForce(force);

                
            }
        }
    }
}

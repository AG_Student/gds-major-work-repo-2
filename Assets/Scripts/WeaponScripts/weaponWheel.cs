﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class weaponWheel : MonoBehaviour
{
    public Image i0;
    public Image i1;
    public Image i2;
    public Image i3;
    public GameObject player;
    public static int number;
    public bool canuse = true;
    // Start is called before the first frame update
    void Start()
    {
        player = ComboManager.Instance.player;
    }

    // Update is called once per frame
    void Update()
    {
        if (canuse)
        {
            if (Input.GetKey(KeyCode.Q))
            {
                i1.enabled = true;
                i1.GetComponentInChildren<Text>().enabled = true;
                i2.enabled = true;
                i2.GetComponentInChildren<Text>().enabled = true;
                i3.enabled = true;
                i3.GetComponentInChildren<Text>().enabled = true;
                player.GetComponent<SlowTimeController>().SlowdownFree();
            }
            if (Input.GetKeyUp(KeyCode.Q))
            {
                i1.enabled = false;
                i1.GetComponentInChildren<Text>().enabled = false;
                i2.enabled = false;
                i2.GetComponentInChildren<Text>().enabled = false;
                i3.enabled = false;
                i3.GetComponentInChildren<Text>().enabled = false;
                number = mouseHover.num;
                player.GetComponent<WeaponManager>().EnableWeapon(number);
                player.GetComponent<SlowTimeController>().InstantSpeedUp();
            }
        }
    }
}

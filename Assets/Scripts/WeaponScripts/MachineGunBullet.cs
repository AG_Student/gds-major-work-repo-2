﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGunBullet : MonoBehaviour
{
    public Rigidbody2D rb;
    public float bulletForce;
    public int damage, targetLayer;
    public GameObject particles;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        AudioManager.Instance.PlayGunShot();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == targetLayer)
        {
            I_Damageable enemy = collision.gameObject.GetComponent<I_Damageable>();
            if(enemy != null)
             enemy.Damage(damage);
        }
        if (collision.gameObject.tag != "HitDetection")
        {
            Instantiate(particles, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
        
    }

    public void SetTrajectory(Vector2 dir)
    {
        dir = dir.normalized;
        rb.AddForce(dir * bulletForce, ForceMode2D.Impulse);
    }

    public void Rotate(Vector3 rot)
    {
        Vector3 dir = rot;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}

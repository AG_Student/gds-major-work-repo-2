﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleGun : RocketLauncher
{
    public bool firing = false, mouseUp = true, mainFire;
    GameObject hookShot, pullShot;
    LineRenderer renderer;
    public float maxRange;
    public GameObject playerTest;

    public GameObject altProjectile, proj;

    [SerializeField]
    float holdCount = 0f;

    private new void Start()
    {
        base.Start();
        renderer = GetComponent<LineRenderer>();
    }



    private void OnDestroy()
    {
        PlayerController.landEvent -= RefillAmmo;
    }
    public void Fire()
    {
        if (mouseUp)
        {
            if (!firing)
            {

                firing = true;
                mouseUp = false;

                hookShot = Instantiate(projectile, shotOrigin.position, Quaternion.identity);
                hookShot.GetComponent<GrappleHook>().player = playerTest;
                hookShot.GetComponent<GrappleHook>().Rotate(mouse.transform.position - transform.position);
                proj = hookShot;
                mainFire = true;
                renderer.endColor = Color.blue;
                renderer.startColor = Color.blue;

                currentAmmo--;
                ui.UpdateAmmo(currentAmmo, maxAmmo);
            }
            else if (firing)
            {
                ClearShots();
            }
        }
    }

    void UseGrapple()
{
    if (holdCount > .6f)
        AltFire();
    else
        Fire();
    mouseUp = true;
    Debug.Log(1);
}


private void Update()
{
    //if (Input.GetMouseButtonUp(0))
    //    mouseUp = true;
    //if (Input.GetMouseButtonDown(1))
    //    AltFire();
    ////

    //if (Input.GetKey(KeyCode.Mouse1))
    //{
    //    holdCount += Time.unscaledDeltaTime;
    //}
    //if (Input.GetKeyUp(KeyCode.Mouse1))
    //{
    //    UseGrapple();
    //    holdCount = 0;
    //}



    if (firing)
    {
        DrawLine();
        if (Vector3.Distance(transform.position, proj.transform.position) > maxRange)
            if (mainFire)
                hookShot.GetComponent<GrappleHook>().Unattach();
            else if (!mainFire)
                pullShot.GetComponent<PullHook>().Unattach();
    }
    else if (!firing)
        ClearLine();


}

public void DrawLine()
{
    renderer.SetPosition(0, this.transform.position);

    renderer.SetPosition(1, proj.transform.position);
}

public void ClearLine()
{
    renderer.SetPosition(0, Vector3.zero);
    renderer.SetPosition(1, Vector3.zero);
}

public void AltFire()
{
    if (!firing)
    {

        firing = true;


        pullShot = Instantiate(altProjectile, shotOrigin.position, Quaternion.identity);
        pullShot.GetComponent<PullHook>().Rotate(mouse.transform.position - transform.position);
        proj = pullShot;
        mainFire = false;
        renderer.endColor = Color.red;
        renderer.startColor = Color.red;

        currentAmmo--;
        ui.UpdateAmmo(currentAmmo, maxAmmo);
    }
    else if (firing)
    {
        ClearShots();

    }
}

public void ClearShots()
{
    if (!mainFire)
    {
        ClearAltShot();
    }
    else if (mainFire)
    {
        ClearMainShot();
    }
}

private void ClearAltShot()
{
    if (pullShot != null && pullShot.GetComponent<PullHook>().attached)
    {
        pullShot.GetComponent<PullHook>().Unattach();
        firing = false;


    }
    else
    {
        if (pullShot != null)
            Destroy(pullShot.gameObject);
        firing = false;


    }
}

private void OnDisable()
{
    firing = false;
    Destroy(proj);
}

void ClearMainShot()
{
    if (hookShot != null && hookShot.GetComponent<GrappleHook>().attached)
    {
        hookShot.GetComponent<GrappleHook>().Unattach();
        firing = false;
        mouseUp = false;

    }
    else
    {
        if (hookShot != null)
            Destroy(hookShot.gameObject);
        firing = false;
        mouseUp = false;

    }
}
}



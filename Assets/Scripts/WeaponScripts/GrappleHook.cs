﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleHook : MissileBehaviour
{
    public List<int> targetLayers;
    public GameObject player;
    public PlayerPhysicsController playerController;
    public bool attached = false, pull = true;
    public float grappleForce = 1;
    public GrappleGun gun;

    public new void Start()
    {
        base.Start();

        player = ComboManager.Instance.player;
        playerController = player.GetComponent<PlayerPhysicsController>();
        gun = player.GetComponentInChildren<GrappleGun>();
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        // if (targetLayers.Contains(collision.gameObject.layer))
        if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy") || collision.gameObject.layer == LayerMask.NameToLayer("GrapplePoint"))
        {
            Attach(collision.gameObject);
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Ground") || collision.gameObject.layer == LayerMask.NameToLayer("Default"))
            Unattach();
    }

     void Attach(GameObject target)
    {
        rb.velocity = Vector2.zero;
        gameObject.transform.parent = target.gameObject.transform;
        attached = true;
        StartCoroutine(ApplyGrappleForce());
        gameObject.layer = 0;

    }
    public void Unattach()
    {
        attached = false;   
        StopCoroutine(ApplyGrappleForce());
        gun.firing = false;
        Destroy(this.gameObject);
    }
    IEnumerator ApplyGrappleForce()
    {
        while (attached)
        {
            Vector3 dir = transform.position - player.transform.position;
            dir = dir.normalized; 
            playerController.rb.velocity = dir * grappleForce;


            yield return null;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            
            pull = false;
            Unattach();
        }

    }


    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboManager : MonoBehaviour
{
    private static ComboManager instance;
    public static ComboManager Instance
    {
        get
        {
            return instance;
        }
    }

    public ComboBar comboMeter;

    public enum ComboLevel
    {
        none,
        D,
        C,
        B,
        A,
        S,
        Hectic
    }
    public ComboLevel comboLevel = ComboLevel.none;
    public Dictionary<string, float> comboThresholds;
    Dictionary<string, float> totalScoreMod;
    public List<Sprite> comboImages;

    public GameObject player;

    public float comboDecayRate, comboDecayDelay, delayTimer;
    public float currentScore;
    public bool decaying = false, isComboing;
    public Image imageHolder;

    public float totalScore = 0f;

    public float scoreCache = 0f;

    private void Start()
    {
        instance = this;
        delayTimer = comboDecayDelay;
        comboThresholds = new Dictionary<string, float>();
        comboThresholds.Add("none", -10000000f);
        comboThresholds.Add("D", 1f);
        comboThresholds.Add("C", 100f);
        comboThresholds.Add("B", 250f);
        comboThresholds.Add("A", 500f);
        comboThresholds.Add("S", 1000f);
        comboThresholds.Add("Hectic", 2000f);

        totalScoreMod = new Dictionary<string, float>();
        totalScoreMod.Add("none", 1f);
        totalScoreMod.Add("D", 1f);
        totalScoreMod.Add("C", 2f);
        totalScoreMod.Add("B", 3f);
        totalScoreMod.Add("A", 4f);
        totalScoreMod.Add("S", 5f);
        totalScoreMod.Add("Hectic", 6f);

        ComboManager.Instance.totalScore = ApplicationData.lastScore;
        //PlayerController.landEvent += StopCombo;

    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.O))
            AddScore(25);

        ComboCheck();
        ComboDecay();
        UpdateBar();
        
    }

    private void ComboCheck()
    {
        if (ReturnNextThreshold() != "Catch")
        {
            if (currentScore >= comboThresholds[ReturnNextThreshold()])
            {
                IncreaseComboGrade();
            }
        }
    }

    public void UpdateBar()
    {
        if (comboLevel == ComboLevel.none)
            imageHolder.gameObject.SetActive(false);
        if (imageHolder.gameObject.activeInHierarchy == false && comboLevel != ComboLevel.none)
            imageHolder.gameObject.SetActive(true);

        if (imageHolder.gameObject.activeInHierarchy)
            comboMeter.currentHP = currentScore;
    }

    private void ComboDecay()
    {
        if (!decaying)
        {
            delayTimer -= Time.deltaTime;
            if (delayTimer <= 0f)
            {
                decaying = true;
                delayTimer = comboDecayDelay;
            }
        }
        else if (decaying)
        {
            if (currentScore > 0)
            {
                currentScore -= Time.deltaTime * (comboDecayRate * (totalScoreMod[ReturnScoreMod()] / 3));
                if (currentScore < comboThresholds[comboLevel.ToString()])
                {
                    ReduceComboGrade();
                }
            }
            else if (currentScore < 0)
                currentScore = 0;
        }
        if(currentScore > 0)
        {
            isComboing = true;
        }
    }

    private void AddScore(float score)
    {
        currentScore += score;
        TrackCombo(score);
        decaying = false;
        delayTimer = comboDecayDelay;
    }
    public void ReduceComboGrade()
    {
        if((int)comboLevel != 0)
        {
            int nextLevel = (int)comboLevel - 1;
            comboLevel = (ComboLevel)nextLevel;
            if (nextLevel != 0)
                ChangeImage(nextLevel - 1);
            else if (nextLevel == 0)
                imageHolder.sprite = null;

            comboMeter.maxHP = comboThresholds[ReturnNextThreshold()];
        }
    }
    public void IncreaseComboGrade()
    {
        if ((int)comboLevel != 6)
        {
            int nextLevel = (int)comboLevel + 1;
            comboLevel = (ComboLevel)nextLevel;
            ChangeImage(nextLevel-1);
            comboMeter.maxHP = comboThresholds[ReturnNextThreshold()];
            
        }
    }
    
    string ReturnNextThreshold()
    {
        if ((int)comboLevel != 6)
        {
            int nextLevel = (int)comboLevel + 1;
            ComboLevel tempLevel = (ComboLevel)nextLevel;
            string levelName = tempLevel.ToString();
            return levelName;
        }
        else
            return "Catch";
    }

    string ReturnScoreMod()
    {

        //int nextLevel = (int)comboLevel ;
        //ComboLevel tempLevel = (ComboLevel)nextLevel;
        //string levelName = tempLevel.ToString();
        //return levelName;
        return comboLevel.ToString();
        
    }

    public void ChangeImage(int num)
    {
        imageHolder.sprite = comboImages[num];
    }

    IEnumerator QueueCombo(float score)
    {
        if(scoreCache == 0f)
            scoreCache += score;
        else
        {
            scoreCache +=  score + (score/2);
        }
        yield return new WaitForSeconds(.1f);
        AddScore(scoreCache);
        scoreCache = 0;

    }
    public void CallCombo(float score)
    {
        StartCoroutine(QueueCombo(score));
    }

    public void StopCombo()
    {
        isComboing = false;
        comboLevel = ComboLevel.none;
        decaying = true;
        currentScore = 0f;
        scoreCache = 0;
        imageHolder.sprite = null;
    }

    void TrackCombo(float score)
    {
        totalScore += score * totalScoreMod[ReturnScoreMod()];
    }

    public void FinaliseScore()
    {
        ApplicationData.score = totalScore;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponManager : MonoBehaviour
{
    AttackController player;
    public GameObject[] weapons = new GameObject[2];
    public FuelGauge fuelGauge;
    private Renderer rightArmRenderer;
    private int cachedWeapon;



    //public WeaponUI uiScript;
    //Image weaponImage;


    int currentWeapon = 0;

    private void Start()
    {
        player = GetComponent<AttackController>();
        rightArmRenderer = player.transform.Find("Right_arm").GetComponent<SpriteRenderer>();
        EnableWeapon(0);
        currentWeapon = 0;
    }

    private void Update()
    {
        if(ApplicationData.canChainsaw)
        EnableChainsaw();

        if (ApplicationData.canChange)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1)){
                EnableWeapon(0);
                rightArmRenderer.enabled = true;
              }
            else if (Input.GetKeyDown(KeyCode.Alpha2)){
                EnableWeapon(2);
                rightArmRenderer.enabled = false;
              }

            if (Input.GetAxis("Mouse ScrollWheel") > 0 || Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                if (currentWeapon == 2)
                {
                    currentWeapon = 0;
                    rightArmRenderer.enabled = true;
                }
                else if (currentWeapon == 0)
                {
                    currentWeapon = 2;
                    rightArmRenderer.enabled = false;
                }
                EnableWeapon(currentWeapon);
                Debug.Log(currentWeapon);
            }
        }
        //if (Input.GetAxis("Mouse ScrollWheel") < 0)
        //{
        //    if (currentWeapon == 0)
        //    {
        //        currentWeapon = 3;
        //    }
        //    EnableWeapon(currentWeapon - 1);
        //}
    }
    public void EnableWeapon(int weapon)
    {
        for (int i = 0; i < weapons.Length; i ++)
        {
            if (i == weapon)
                weapons[i].SetActive(true);
            else
                weapons[i].SetActive(false);
        }
       player.ChangeWeapon(weapons[weapon]);
       //ChangeWeaponUI(weapons[weapon]);
        currentWeapon = weapon;
    }

    public void EnableChainsaw()
    {
        if (Input.GetKey(KeyCode.Q) && fuelGauge.active)
        {
            if (currentWeapon != 1)
            {
                cachedWeapon = currentWeapon;
            }
            EnableWeapon(1);
            AudioManager.Instance.PlayChainsaw(true);
            rightArmRenderer.enabled = true;

        }
        else if ((Input.GetKeyUp(KeyCode.Q)))
        {
            EnableWeapon(cachedWeapon);
            AudioManager.Instance.PlayChainsaw(false);
        }
            
            
       
            //armRenderer.enabled = false;
    }
    //public void ChangeWeaponUI(GameObject weapon)
    //{
    //    uiScript.ApplyWeapon(weapon);
    //}


}

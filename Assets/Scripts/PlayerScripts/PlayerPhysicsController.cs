﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPhysicsController : MonoBehaviour
{
    public Rigidbody2D rb;
    public PlayerController player;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GetComponent<PlayerController>();
    }

    private void FixedUpdate()
    {
        if (transform.position.z != 0)
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }

    public void ApplyForce(Vector3 force)
    {
        rb.AddForce(force, ForceMode2D.Impulse);
    }
}

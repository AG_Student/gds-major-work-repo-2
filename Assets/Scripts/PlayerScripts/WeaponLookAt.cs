﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponLookAt : GunLookat
{
   public GameObject mousePos;
   public SpriteRenderer gunSprite;
   protected Vector3 direction;
   private float xPos, yPos, zPos;
   private float shotXPos, shotYPos, shotZPos;

   // Start is called before the first frame update
   public override void Start()
   {
        target = mousePos.transform;
        gunSprite = gameObject.GetComponent<SpriteRenderer>();
        xPos = gunSprite.transform.localPosition.x;
        yPos = gunSprite.transform.localPosition.y;
        zPos = gunSprite.transform.localPosition.z;
        shotXPos = gameObject.transform.Find("shotOrigin").localPosition.x;
        shotYPos = gameObject.transform.Find("shotOrigin").localPosition.y;
        shotZPos = gameObject.transform.Find("shotOrigin").localPosition.z;
    }

    // Update is called once per frame
   protected void Update()
    {
        direction = target.position - transform.position;
        direction = direction.normalized;
        if (direction.x < 0)
        {
            gunSprite.flipY = true;
            gunSprite.transform.localPosition = new Vector3(xPos * -1, yPos, zPos);
            gameObject.transform.Find("shotOrigin").localPosition = new Vector3(shotXPos, shotYPos * -1, shotZPos);
        }
        else if (direction.x >= 0)
        {
            gunSprite.flipY = false;
            gunSprite.transform.localPosition = new Vector3(xPos, yPos, zPos);
            gameObject.transform.Find("shotOrigin").localPosition = new Vector3(shotXPos, shotYPos, shotZPos);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    public I_Weapon currentWeapon;
    public CursorController mouse;

    private void Update()
    {

        if (Input.GetMouseButton(0))
        {
            currentWeapon.Attack();
        }
    }

    public void ChangeWeapon(GameObject weapon)
    {
        currentWeapon = weapon.GetComponent<I_Weapon>();
    }

}

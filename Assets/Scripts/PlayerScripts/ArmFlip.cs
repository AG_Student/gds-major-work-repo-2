﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmFlip : MonoBehaviour
{
    public GameObject mousePos;
    public SpriteRenderer armSprite;
    protected Vector3 direction;
    protected Transform target;
    private float xPos;
    private float yPos;
    private float zPos;
    // Start is called before the first frame update

    void Start()
    {
        target = mousePos.transform;
        armSprite = gameObject.GetComponent<SpriteRenderer>();
        xPos = armSprite.transform.localPosition.x;
        yPos = armSprite.transform.localPosition.y;
        zPos = armSprite.transform.localPosition.z;
    }

    void Update()
    {
        direction = target.position - transform.position;
        direction = direction.normalized;
        if (direction.x < 0)
        {
            armSprite.flipX = true;
            armSprite.transform.localPosition = new Vector3(xPos * -1, yPos, zPos);
        }
        else if (direction.x >= 0)
        {
            armSprite.flipX = false;
            armSprite.transform.localPosition = new Vector3(xPos, yPos, zPos);
        }
    }
}

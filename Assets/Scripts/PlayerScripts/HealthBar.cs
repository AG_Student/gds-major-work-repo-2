﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    //script for health bar

    public Transform bar;

    public float maxHP;
    public float currentHP;

   // public GameObject manager;

    // Start is called before the first frame update
    void Start()
    {

        maxHP = GamePlayManager.Instance.maxHealth;
        SetSize(maxHP);

        currentHP = maxHP;
        
    }

    private void Update()
    {
        GamePlayManager.Instance.currentPlayerHealth = currentHP;
        SetSize(currentHP);
    }

    public void SetSize(float HP)
    {
        currentHP = HP;
        if (currentHP >= 0)
        {
            bar.localScale = new Vector3(currentHP / maxHP, 1f);
        }
        else
        {
            bar.localScale = new Vector3(0, 1f);
        }
    
        
    }
    
}

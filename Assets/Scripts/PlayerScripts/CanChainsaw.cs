﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanChainsaw : MonoBehaviour
{
    //private void Awake()
    //{
    //    if (SceneManager.GetActiveScene().name != "SampleScene")
    //    {
    //        ApplicationData.canChainsaw = true;
    //    }

    //    else if (SceneManager.GetActiveScene().name == "SampleScene")
    //    {
    //        ApplicationData.canChainsaw = false;
    //    }
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            ApplicationData.canChainsaw = true;
        }
    }
}

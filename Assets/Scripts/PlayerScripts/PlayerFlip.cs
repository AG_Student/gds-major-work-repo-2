﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFlip : MonoBehaviour
{
    public GameObject mousePos;
    public SpriteRenderer gunSprite;
    protected Vector3 direction;
    protected Transform target;
    // Start is called before the first frame update

    void Start()
    {
        target = mousePos.transform;
        gunSprite = gameObject.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        direction = target.position - transform.position;
        direction = direction.normalized;
        if (direction.x < 0)
        {
            gunSprite.flipX = true;
        }
        else if (direction.x >= 0)
        {
            gunSprite.flipX = false;
        }
    }
}

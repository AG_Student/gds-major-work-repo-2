﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowTimeController : MonoBehaviour
{
    //Script for controlling slowing time, both through player input and other game actions

    public bool canUse = true;
    public ComboBar slowBar;
    public float slowTimeAmount = .15f, slowTimeResource = 10f, slowTimeMax = 10f, speedUpRate = 2f; //resources
    public bool slowingTime = false;

    public PauseMenu pauseMenu;

    private void Start()
    {
        slowBar.maxHP = slowTimeMax;
        slowBar.currentHP = slowTimeResource;
    }


    // Update is called once per frame
    void Update()
    {
        if(canUse)
            HandleInput();
        if (slowingTime)
            ReduceGauge();
    }

    //Receives input from the player
    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Slowdown();
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            if (slowingTime && !pauseMenu.gamePaused)
            {
                StartCoroutine("SpeedUp");
                StartCoroutine("RestoreGauge");
            }
             
        }
    }

    //reduces the gauge and updates the UI
    private void ReduceGauge()
    {
        slowTimeResource -= Time.deltaTime * 2;
        if (slowTimeResource <= 0)
        {
            StartCoroutine("SpeedUp");
            StartCoroutine("RestoreGauge");
        }
        UpdateUI();
    }

    //slows time
    public void Slowdown()
    {
        if (!slowingTime)
        {
            Time.timeScale = slowTimeAmount;
            // Time.fixedDeltaTime = (Time.timeScale * .02f) * 2; //for physics
            Time.fixedDeltaTime = Time.timeScale * .02f;
            slowingTime = true;
        }

    }
    public void SlowdownFree()
    {
        if (!slowingTime)
        {
            Time.timeScale = slowTimeAmount;
            // Time.fixedDeltaTime = (Time.timeScale * .02f) * 2; //for physics
            Time.fixedDeltaTime = Time.timeScale * .02f;
            
        }

    }

    public void StartSpeedUp()
    {
        StartCoroutine("SpeedUp");
    }



    //begins returning time to normal
    IEnumerator SpeedUp()
    {

        slowingTime = false;
        InstantSpeedUp();
        while (Time.timeScale < 1f)
        {
            if (slowingTime)
            {
                yield break; //cancels the coroutine if the player starts slowing time again
            }
            Time.timeScale += Time.deltaTime * speedUpRate;
            //   Time.fixedDeltaTime = (Time.timeScale * .02f) * 2;
            Time.fixedDeltaTime = Time.timeScale * .02f;

            yield return null;
        }
        //  Time.fixedDeltaTime = .02f;
    }

    public void InstantSpeedUp()
    {
        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.timeScale * .02f;
    }

    //restores the gauge and updates the UI
    IEnumerator RestoreGauge()
    {
        while (!slowingTime && slowTimeResource <= slowTimeMax)
        {
            slowTimeResource += Time.deltaTime / 2;
            UpdateUI();
            yield return null;
        }
    }

    public void UpdateUI()
    {
        slowBar.currentHP = slowTimeResource;
    }

}

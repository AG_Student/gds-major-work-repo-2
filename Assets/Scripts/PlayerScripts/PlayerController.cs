﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour, I_Damageable
{

    [Header("Horizontal Movement")]
    public float moveSpeed = 10f;
    public float oldmoveSpeed;
    private float horizontalMovement;
    public Vector2 direction;

    [Header("Vertical Movement")]
    public float jumpSpeed = 15f;
    public float jumpDelay = 0.25f;
    private float jumpTimer;


    [Header("Components")]
    public Rigidbody2D rb;
    public LayerMask groundLayer;

    [Header("Physics")]
    public float maxSpeed = 7f;
    public float oldMaxSpeed;
    public float linearDrag = 4f;
    public float gravity = 1;
    public float fallMultiplier = 5;
    public float jumpHeight = 5f;

    public float airControl;

    [Header("Air Dodge")]
    public float dodgeForce;
    public float dodgeTimeMax;
    float dodgeTime;
    public bool canDodge = true;

    [Header("Collision")]
    public bool OnGround = false;
    public float groundLength = 0.6f;
    public Vector3 colliderOffset;

    [Header("sound effects")]
    public AudioClip smallJump;
    public AudioClip bigJump;

    [Header("check")]
    public bool follow;

    [Header("Animator")]
    private Animator anim;

    [Header("Health")]
    public GameObject healthBar;
    public HealthBar bar;

    PlayerPhysicsController physicsController;
    WeaponManager weaponManager;

    public delegate void LandEvent();
    public static event LandEvent landEvent;
    bool isDodging = false;

    public bool canUse = true;

    public bool canMove = true;

    bool spawnDust = false;

    public float invincibilitylength=1;
    public float invincibilityCounter;
    public SpriteRenderer playerRenderer;
    private float flashCounter;
    public float flashLength = 0.1f;

    public GameObject dustParticles;
    public TrailRenderer trail;

    public CinemachineVirtualCamera c_camera;
    CinemachineFramingTransposer cam;



    // Start is called before the first frame update
    void Start()
    {
        weaponManager = GetComponent<WeaponManager>();
        physicsController = GetComponent<PlayerPhysicsController>();
        rb = GetComponent<Rigidbody2D>();
        oldMaxSpeed = maxSpeed;
        oldmoveSpeed = moveSpeed;
        bar = healthBar.GetComponent<HealthBar>();
        // ComboManager.Instance.player = this.gameObject;
        trail = GetComponentInChildren<TrailRenderer>();
        cam = c_camera.GetCinemachineComponent<CinemachineFramingTransposer>();

        if (ApplicationData.spawnLocation != null)
            transform.position = ApplicationData.spawnLocation;

    }

    private void Awake()
    {


    }

    // Update is called once per frame
    void Update()
    {
        if(canUse)
            PlayerInput();

        OnGround = Physics2D.Raycast(transform.position + colliderOffset, Vector2.down, groundLength, groundLayer) || Physics2D.Raycast(transform.position - colliderOffset, Vector2.down, groundLength, groundLayer);
        if (OnGround)
        {
            canDodge = true;
            trail.emitting = false;
            //landEvent();
            ComboManager.Instance.comboDecayRate = 20;

            AnimationManager.Instance.isGroundedTrue();

            if (spawnDust)
            {
                Instantiate(dustParticles, transform.position, Quaternion.identity);
                spawnDust = false;
            }

        }
        else if (!OnGround)
        {
            ComboManager.Instance.comboDecayRate = 5;
            AnimationManager.Instance.isGroundedFalse();
            trail.emitting = canDodge;
            spawnDust = true;
        }

        if(invincibilityCounter > 0)
        {
            invincibilityCounter -= Time.deltaTime;
            flashCounter -= Time.deltaTime;
            if (flashCounter <= 0)
            {
                playerRenderer.enabled = !playerRenderer.enabled;
                flashCounter = flashLength;
            }
            if (invincibilityCounter <= 0)
            {
                playerRenderer.enabled = true;
            }
        }

       ForwardInputCheck();
        JumpAnim();
        Death();
        CameraTrackController();

    }

    private void FixedUpdate()
    {
        if (canMove)
            MoveCharacter(direction.x);

        if (jumpTimer > Time.time && OnGround)
        {
            Jump();
        }

        if(canMove)
            ModifyPhysics();
        PreventSlide();

        if (Input.GetKeyDown(KeyCode.K)) 
        {
            RestartButton();
        }
    }

    public void RefillAmmo()
    {
        landEvent();
    }

    public void ForwardInputCheck()
    {
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            follow = true;

        }
        else
        {
            follow = false;
        }
    }

    // gets player input for jumping and moving, sets the jump timer to allow for hangtime
    void PlayerInput()
    {
        direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        horizontalMovement = rb.velocity.x;
        AnimationManager.Instance.setSpeed(horizontalMovement);
        //anim.SetFloat("Speed", Mathf.Abs(horizontalMovement));

        //if (Input.GetKeyDown(KeyCode.D))
        //{
        //    //RotateRight();
        //}
        //else if (Input.GetKeyDown(KeyCode.A))
        //{
        //    RotateLeft();
        //}


        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpTimer = Time.time + jumpDelay;

        }

        if(Input.GetKeyDown(KeyCode.Space) && !OnGround)
        {
            if (canDodge)
            {
                AirDodge(direction);
                StartCoroutine(AirDodgeTimer());
            }
        }


        //if (Input.GetKeyDown(KeyCode.LeftShift))
        //{
        //    moveSpeed = moveSpeed * 2;
        //    maxSpeed = maxSpeed * 2;
        //    Debug.Log(moveSpeed);
        //}
        //else if (Input.GetKeyUp(KeyCode.LeftShift))
        //{
        //    moveSpeed = oldmoveSpeed;
        //    maxSpeed = oldMaxSpeed;
        //    Debug.Log(moveSpeed);
        //}
    }

    IEnumerator AirDodgeTimer()
    {
        Vector2 velocityTemp = rb.velocity;
        isDodging = true;
        //canDodge = false;
        float gravityTemp = rb.gravityScale;
        rb.gravityScale = 0f;
        while(dodgeTime > 0f)
        {
            dodgeTime -= Time.deltaTime;
            yield return null;
        }
        dodgeTime = dodgeTimeMax;
        isDodging = false;
       // canDodge = true;
        rb.gravityScale = gravityTemp;
        yield return null;
    }

    void AirDodge(Vector3 dir)
    {
        rb.velocity = Vector2.zero;
        if (dir != Vector3.zero)
        {
            if (dir.y == 0)
            {
                dir.y = 1f;
                dir.x = dir.x * 1.3f;
            }
            rb.AddForce(dir * dodgeForce, ForceMode2D.Impulse);
        }
        else if (dir == Vector3.zero)
            rb.AddForce(Vector2.up * dodgeForce, ForceMode2D.Impulse);

        canDodge = false;
    }

    void RotateLeft()
    {
        transform.eulerAngles = new Vector3(0, 180, 0);
    }


    void RotateRight()
    {
        transform.eulerAngles = new Vector3(0, 0, 0);
    }

    void JumpAnim()
    {

        if (Input.GetKey(KeyCode.Space) || OnGround == false)
        {

            //anim.SetBool("Jumping", true);
        }
        else
        {
         //   anim.SetBool("Jumping", false);
        }
    }

    // character movement physocs
    void MoveCharacter(float horizontal)
    {
        if (OnGround)
        {
            if (horizontal != 0f)
                rb.velocity = new Vector3(horizontal * moveSpeed, rb.velocity.y, 0f);

            if (Mathf.Abs(rb.velocity.x) > maxSpeed)
            {
                rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);
            }
        }
        else if (!OnGround)
        {
            if(!isDodging)
                rb.AddForce(Vector2.right * horizontal * moveSpeed * airControl);
              // rb.velocity = new Vector3(horizontal * moveSpeed, rb.velocity.y, 0f);
                //rb.velocity = new Vector2(horizontal * moveSpeed, rb.velocity.y);
            else if (isDodging && horizontal != 0f)
                rb.AddForce(Vector2.right * horizontal * moveSpeed * airControl);
            //rb.velocity = new Vector3(horizontal * moveSpeed, rb.velocity.y, 0f);

            if (Mathf.Abs(rb.velocity.x) > maxSpeed)
            {
                rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);
            }

        }

    }

    // jump function
    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(Vector2.up * jumpSpeed, ForceMode2D.Impulse);
        jumpTimer = 0;
        Instantiate(dustParticles, transform.position, Quaternion.identity);


    }

    public void JumpReset()
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
    }

    // physics modifiers to make things feel better
    void ModifyPhysics()
    {
        bool changeDirections = (direction.x > 0 && rb.velocity.x < 0) || (direction.x < 0 && rb.velocity.x > 0);


        if (OnGround)
        {


        }
        else
        {
            // gravity manipulation based on of the player is holding down space, allows for the variable jump
            rb.gravityScale = gravity;
            rb.drag = linearDrag * 0.15f;

            if (rb.velocity.y < 0)
            {
                //rb.gravityScale = gravity * fallMultiplier;
            }
            else if (rb.velocity.y > 0 && !Input.GetKey(KeyCode.Space))
            {
                rb.gravityScale = gravity * fallMultiplier;
            }

        }
    }

    private void RestartButton() 
    {
            this.GetComponent<Respawn>().respawn();
            healthBar.GetComponent<HealthBar>().currentHP = healthBar.GetComponent<HealthBar>().maxHP;
            //Destroy(this.gameObject);

    }

    public void Death()
    {
        if (healthBar.GetComponent<HealthBar>().currentHP <= 0 && SceneManager.GetActiveScene().name != "SampleScene 1")
        {
            this.GetComponent<Respawn>().respawn();
            healthBar.GetComponent<HealthBar>().currentHP = healthBar.GetComponent<HealthBar>().maxHP;
            //Destroy(this.gameObject);
        }
        else if (healthBar.GetComponent<HealthBar>().currentHP <= 0 && SceneManager.GetActiveScene().name == "SampleScene 1") 
        {
            ComboManager.Instance.FinaliseScore();
            SceneManager.LoadScene("win");
        }
    }

    public void LifeMan()
    {
        gameObject.GetComponent<Collider2D>().enabled = true;
    }

    //debug lines
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position + colliderOffset, transform.position + colliderOffset + Vector3.down * groundLength);
        Gizmos.DrawLine(transform.position - colliderOffset, transform.position - colliderOffset + Vector3.down * groundLength);
    }

    public void Damage(int damage)
    {
        if (invincibilityCounter <= 0)
        {
            bar.currentHP -= damage;
            playVoiceLine();
            AudioManager.Instance.PlayHit();
            if(bar.currentHP == 1)
            {
                AudioManager.Instance.PlayLowHP();
            }
            // Destroy(this.gameObject);
            Debug.Log("hit");
            ComboManager.Instance.StopCombo();
            if (bar.currentHP != 0)
            {
                invincibilityCounter = invincibilitylength;
                playerRenderer.enabled = false;
                flashCounter = flashLength;
            }
        }
    }

    public void playVoiceLine()
    {
        int i = Random.Range(0, 100);
        Debug.Log(i);
        if (i >= 65)
        {
            AudioManager.Instance.randomVoiceLine();
        }
    }

    public void DamageOverride(int damage)
    {
        bar.currentHP -= damage;
        if (bar.currentHP == 1)
        {
            AudioManager.Instance.PlayLowHP();
        }
        // Destroy(this.gameObject);
        Debug.Log("hit");
        ComboManager.Instance.StopCombo();
        if (bar.currentHP != 0)
        {
            invincibilityCounter = invincibilitylength;
            playerRenderer.enabled = false;
            flashCounter = flashLength;
        }
    }

        void PreventSlide()
    {
        if (OnGround)
        {
            if(direction.x == 0)
            {
                rb.velocity = new Vector2(0, rb.velocity.y);
            }
        }
    }

    public void DodgeReset()
    {
        if (!canDodge)
            canDodge = true;
    }

    void CameraTrackController()
    {
        //if (!OnGround)
        //{
        //    cam.m_LookaheadIgnoreY = false;
        //}
        //else if (OnGround)
        //{
        //    cam.m_LookaheadIgnoreY = true;
        //}
    }

}

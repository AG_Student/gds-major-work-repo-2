﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaDoor : MonoBehaviour
{
    public bool locked = false;
    public Material doorShader;
    public SpriteRenderer sRenderer;

    // Start is called before the first frame update
    void Start()
    {
        LockDoor();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator LockDoorAnim()
    {
        Debug.Log(doorShader.GetFloat("_timeValue"));
        while(doorShader.GetFloat("_timeValue") > -1f)
        {
            float i = doorShader.GetFloat("_timeValue");
            i -= Time.deltaTime;
            doorShader.SetFloat("_timeValue", i);
            yield return null;
        }
        doorShader.SetFloat("_TimeValue", 1f);
        sRenderer.enabled = true;
    }

    IEnumerator UnLockDoorAnim()
    {

        sRenderer.enabled = false;
        while (doorShader.GetFloat("_timeValue") < 1f)
        {
            float i = doorShader.GetFloat("_timeValue");
            i += Time.deltaTime;
            doorShader.SetFloat("_timeValue", i);
            yield return null;
        }
        doorShader.SetFloat("_timeValue", 1f);
        gameObject.SetActive(false);
    }

    public void LockDoor()
    {
        StartCoroutine(LockDoorAnim());
        GamePlayManager.Instance.SetLightsRed();
        locked = true;
        //Do stuff
    }

    public void UnlockDoor()
    {
        GamePlayManager.Instance.SetLightsYellow();
        Debug.LogWarning(100000000);
        StartCoroutine(UnLockDoorAnim());
    }
}

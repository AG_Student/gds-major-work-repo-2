﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuelGauge : HealthBar
{
    public bool refuel = false, active = true;

    public Image sprite, bg;
    public float fuel, maxFuel;
    bool currentlyUsing = false;
    public ChainsawScript chainsaw;

    private void Start()
    {

        fuel = chainsaw.fuel;
        maxFuel = fuel;
        maxHP = maxFuel;
    }


    private void Update()
    {
        currentHP = fuel;
        if (fuel <= 0f)
            StopUsing();
        else if (fuel < maxFuel && !currentlyUsing)
            refuel = true;

        if (fuel >= maxFuel)
        {
            sprite.enabled = false;
            bg.enabled = false;
        }
        else
        {
            sprite.enabled = true;
            bg.enabled = true;
        }


        RefuelMethod();
        SetSize(currentHP);

    }

    private void RefuelMethod()
    {
        if (fuel < 0f)
            fuel = 0f;
        if (fuel < maxFuel)
            fuel += Time.deltaTime / 2;
        else if (fuel > maxFuel)
            fuel = maxFuel;


        if (fuel >= maxFuel)
        {
            refuel = false;
            chainsaw.active = true;
            active = true;
             sprite.color = Color.green;
        }
    }

    void StopUsing()
    {
        refuel = true;
        chainsaw.active = false;
        active = false;
         sprite.color = Color.red;
    }
}

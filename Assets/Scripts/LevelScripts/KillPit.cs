﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPit : MonoBehaviour
{

    public int damageDealt;
    public float bounceForce;

     
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            AudioManager.Instance.PlayBurning();
            collision.gameObject.GetComponent<PlayerController>().DamageOverride(damageDealt);
            PlayerPhysicsController player = collision.gameObject.GetComponent<PlayerPhysicsController>();
            player.rb.velocity = Vector2.zero;
            player.ApplyForce((player.transform.position - transform.position) * bounceForce);
        }
    }
}

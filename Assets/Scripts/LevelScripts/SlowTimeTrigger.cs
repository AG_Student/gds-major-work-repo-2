﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowTimeTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && collision.gameObject.layer != 13)
            collision.gameObject.GetComponent<SlowTimeController>().Slowdown();
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSet : MonoBehaviour
{

    public ApplicationData.CurrentLevel level;

    // Start is called before the first frame update
    void Start()
    {
        ApplicationData.currentLevel = level;
    }
     
}

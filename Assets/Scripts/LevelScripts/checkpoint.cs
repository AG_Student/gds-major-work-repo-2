﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkpoint : MonoBehaviour
{
    GameObject player;
    Animator anim;
    public bool on;
    // Start is called before the first frame update
    void Start()
    {
        player = ComboManager.Instance.player;
        anim = this.GetComponent<Animator>();
        on = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            player.GetComponent<PlayerController>().RefillAmmo();
            if (on == false)
            {
                collision.GetComponent<Respawn>().updateLocation(this.transform.position);
                on = true;
            }

            anim.SetTrigger("Player_touch");
            ApplicationData.spawnLocation = transform.position;
            ApplicationData.lastScore = ComboManager.Instance.totalScore;
        }
    }

}

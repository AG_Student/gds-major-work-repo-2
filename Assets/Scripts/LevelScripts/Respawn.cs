﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    public Vector3 respawnLocation;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = ComboManager.Instance.player;
        respawnLocation = player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void updateLocation(Vector3 pos)
    {
        respawnLocation = pos;
    }

    public void respawn()
    {
        player.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        player.transform.position = respawnLocation;
        GamePlayManager.Instance.ReloadScene();

    }
}

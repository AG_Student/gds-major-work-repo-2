﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessLevel : MonoBehaviour
{
    private const float PLAYER_DISTANCE_SPAWN_LEVEL_PART = 15f;

    [SerializeField] private Transform levelPart_Start;
    [SerializeField] private List<Transform> levelPartList;
    [SerializeField] private PlayerController player;

    int levelInt = 0;

    private Vector3 lastEndPostition;

   

    private void Awake()
    {
        lastEndPostition = levelPart_Start.Find("EndPosition").position;
        SpawnLevelPart();

        int startingSpawnLevelParts = 5;
        for (int i = 0; i < startingSpawnLevelParts; i++) 
        {
            SpawnLevelPart();
        }


    }

    private void Update()
    {
        if (Vector3.Distance(player.transform.position, lastEndPostition) < PLAYER_DISTANCE_SPAWN_LEVEL_PART) 
        {
            SpawnLevelPart();
        }
    }

    private void SpawnLevelPart()
    {
        int temp = Random.Range(0, levelPartList.Count);
        
        if (temp == levelInt)
        {
            while (temp == levelInt)
            {
                temp = Random.Range(0, levelPartList.Count);
                break;
            }
        }
        levelInt = temp;
        
            Transform chosenLevelPart = levelPartList[levelInt];
       Transform lastLevelPartTransform = SpawnLevelPart(chosenLevelPart, lastEndPostition);
        Debug.Log(lastLevelPartTransform.Find("EndPosition").position);
        lastEndPostition = lastLevelPartTransform.Find("EndPosition").position;
    }

    private Transform SpawnLevelPart(Transform levelPart, Vector3 spawnPosition) 
    {
       Transform levelPartTransform = Instantiate(levelPart, spawnPosition, Quaternion.identity);
        return levelPartTransform;
    }
}

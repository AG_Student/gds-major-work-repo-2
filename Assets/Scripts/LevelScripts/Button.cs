﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Button : MonoBehaviour
{
    // Start is called before the first frame update
    public void Restart()
    {
        ApplicationData.score = 0f;
        ApplicationData.lastScore = 0f;

        switch (ApplicationData.currentLevel)
        {
            case (ApplicationData.CurrentLevel.level1):
                SceneManager.LoadScene("SampleScene");
                break;
            case (ApplicationData.CurrentLevel.level2):
                SceneManager.LoadScene("Level2");
                break;
            case (ApplicationData.CurrentLevel.levelEndless):
                SceneManager.LoadScene("SampleScene 1");
                break;

        }

        
    }

    public void EndGame()
    {
        SceneManager.LoadScene("MainMenu");
    }
}

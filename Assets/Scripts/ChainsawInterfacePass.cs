﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainsawInterfacePass : MonoBehaviour, I_Weapon
{
    ChainsawScript chainsaw;

    // Start is called before the first frame update
    void Awake()
    {
        chainsaw = GetComponentInChildren<ChainsawScript>();

    }

    public void Attack()
    {
        chainsaw.Attack();
    }
    public int ReturnAmmo()
    {
        return chainsaw.ReturnAmmo();
    }
    public int ReturnCurrentAmmo()
    {
        return chainsaw.ReturnCurrentAmmo();
    }

    public int ReturnMaxAmmo()
    {
        return chainsaw.ReturnMaxAmmo();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ScreenShakeController : MonoBehaviour
{
    CinemachineVirtualCamera cam;
    CinemachineBasicMultiChannelPerlin noise;

    static ScreenShakeController instance;
    public static ScreenShakeController Instance
    {
        get 
        {    
            return instance;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        cam = GetComponent<CinemachineVirtualCamera>();
        noise = cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    // Update is called once per frame


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
            ShakeCamera(3);
    }
    public void ShakeCamera(float shakeAmount)
    {
        StartCoroutine(ShakeCam(shakeAmount));
    }

    IEnumerator ShakeCam(float shakeAmount)
    {

        while (shakeAmount > 0f)
        {
            noise.m_AmplitudeGain = shakeAmount;
            noise.m_FrequencyGain = shakeAmount;
            shakeAmount -= Time.deltaTime*3;
            yield return null;
        }
        noise.m_AmplitudeGain = 0f;
        noise.m_FrequencyGain = 0f;

    }
}

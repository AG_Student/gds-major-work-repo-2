﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface I_Enemy 
{
    void Stun();
    void Knockback(Vector2 force);

    void AddToWave(WaveManager _manager);
}

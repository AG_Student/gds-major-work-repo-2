﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enabler : MonoBehaviour
{
    public GameObject parent;
    public bool trigger;
    // Start is called before the first frame update
    void Start()
    {
        trigger = false;    
    }

    // Update is called once per frame
    void Update()
    {
        if(trigger == true)
        {
            parent.GetComponentInParent<EnemyBounce>().enabled = true;
        }
        else
        {
            parent.GetComponentInParent<EnemyBounce>().enabled = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            trigger = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            trigger = false;
        }
    }
}

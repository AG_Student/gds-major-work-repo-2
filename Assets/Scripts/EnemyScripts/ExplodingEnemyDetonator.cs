﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingEnemyDetonator : MonoBehaviour
{

    private ExplodingEnemy parent;

    public enum Command { Detonate, Chase }

    public Command selectedCommand;

    // Start is called before the first frame update
    void Start()
    {
        parent = gameObject.GetComponentInParent<ExplodingEnemy>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            switch (selectedCommand)
            {
                //case Command.Detonate:
                //    parent.Detonate();
                //    break;
                case Command.Chase:
                    parent.StartChase(collision.gameObject.transform);
                    break;
            }
            
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            switch (selectedCommand)
            {
                case Command.Detonate:
                    parent.StartChase(collision.gameObject.transform);
                    break;
                case Command.Chase:
                    parent.ReturnToIdle();
                    break;
            }

        }

    }
}

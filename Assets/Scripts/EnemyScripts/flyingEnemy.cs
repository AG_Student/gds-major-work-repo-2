﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flyingEnemy : BasicEnemy, I_Damageable
{

    public float move = 3, retMove = 3;

  
    //float attackTimerMax;

    public bool moveChange = false;

    public GameObject child;
    public Transform target;
    public bool Freeze = false;

    // Start is called before the first frame update
    new void Start()
    {

        base.Start();
        target = player.transform;
    }
    public void freeze()
    {
        Freeze = true;
        retSpeedChange();
        moveSpeedChange();
    }

    public void retSpeedChange()
    {
        retMove = Random.Range(1, 4);
    }
    public void moveSpeedChange()
    {
        move = Random.Range(3, 8);
    }
    public void fastChange()
    {
        move = -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Freeze)
        {
            GetTarget();
            if (moveChange)
                updateMoveSpeed();
            else
            {
                moveSpeedChange();
                retSpeedChange();
            }
        }
    }
    void GetTarget()
    {
        if (canMove)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
        }
        //moveVector = player.transform.position - transform.position;
        if (player.transform.position.x > this.transform.position.x)
        {
            this.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

    }
    private void FixedUpdate()
    {

        //Move(moveVector);
    }
    void updateMoveSpeed()
    {
        if (move <= 0)
        {
            moveSpeed = 2f;
            //Attack();
            returnMove();
        }
        else
        {
            move -= Time.deltaTime;
        }
    }

    void returnMove()
    {
        if (retMove <= 0)
        {
            notAttack();
        }
        else
        {
            retMove -= Time.deltaTime;
        }
    }

    void Move(Vector2 dir)
    {
        dir = dir.normalized;
        dir = dir * moveSpeed;
        rb.velocity = dir;
    }

    void Attack()
    {
        //child.GetComponent<MeshRenderer>().enabled = true;
        //child.GetComponent<BoxCollider2D>().enabled = true;
    }

    void notAttack()
    {
        moveSpeed = 1f;
        moveSpeedChange();
        retSpeedChange();
        //child.GetComponent<MeshRenderer>().enabled = false;
        //child.GetComponent<BoxCollider2D>().enabled = false;
    }


}

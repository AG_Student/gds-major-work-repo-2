﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingEnemy : MonoBehaviour, I_Damageable
{

    [Header("Explosion")]
    public GameObject explosion;
    public float explosionRadius;

    [Header("Movement")]
    public float slowSpeed;
    public float fastSpeed;
    public float jumpHeight;
    public float jumpDetectionDistance;

    private Rigidbody2D rigidBody;
    private Vector3 movementDirecton;
    private float movementSpeed;

    [Header("Player Info")]
    public LayerMask playerLayer;
    public float bounceForce;

    private Transform playerTransform;
    private float distFromPlayer;

    public enum EnemyMode { Idle, Moving, Detonated, Exploding, Jumping }
    public EnemyMode currentMode;

    public bool canExplode = false;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        currentMode = EnemyMode.Idle;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(currentMode == EnemyMode.Moving)
        {
            CheckPlayerPosition();
            Move();
        }
    }

    public void Damage(int damage)
    {
        Explode();
    }

    public void StartChase(Transform player)
    {
        playerTransform = player;
        currentMode = EnemyMode.Moving;
        Debug.Log(gameObject.name + " status changed: " + currentMode);
    }

    public void ReturnToIdle()
    {
        currentMode = EnemyMode.Idle;
        Debug.Log(gameObject.name + " status changed: " + currentMode);
    }

    private void CheckPlayerPosition()
    {
        distFromPlayer = playerTransform.position.x - transform.position.x;

        //Sets movement direction
        if (distFromPlayer < 0)
        {
            movementDirecton = Vector3.left;
        }
        else
        {
            movementDirecton = Vector3.right;
        }

        //Sets movement speed. Increases when close to player
        if (Mathf.Abs(distFromPlayer) < 2f)
        {
            movementSpeed = fastSpeed;
        }
        else
        {
            movementSpeed = slowSpeed;
        }

        //If close to the player and they're both not on the ground, jump
        if (Mathf.Abs(distFromPlayer) < jumpDetectionDistance && Mathf.Abs(playerTransform.position.y - transform.position.y) > 0.2f)
        {
            Jump();
        }


    }

    private void Move()
    {
        transform.position += movementDirecton * movementSpeed * Time.deltaTime;

      
    }

    public void Jump()
    {
        if(currentMode != EnemyMode.Jumping)
        {
            currentMode = EnemyMode.Jumping;
            Debug.Log(gameObject.name + " jumped.");
            //rigidBody.velocity = new Vector3(movementDirecton.x, 2) * jumpHeight;
            rigidBody.velocity = Vector3.Normalize(playerTransform.position - transform.position) * jumpHeight;
            canExplode = true;
        }

    }

    public void Explode()
    {
        currentMode = EnemyMode.Exploding;
        Debug.Log(gameObject.name + " status changed: " + currentMode);

        Collider2D player = Physics2D.OverlapCircle(gameObject.transform.position, explosionRadius, playerLayer);

        if(player != null)
        {
            player.gameObject.GetComponent<PlayerController>().Damage(1);
            Debug.Log("Player Hit");
        }

        Instantiate(explosion, transform.position, Quaternion.identity);
        AudioManager.Instance.PlayEnemyExplosion();
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && canExplode)
        {
            //PlayerPhysicsController player = collision.gameObject.GetComponent<PlayerPhysicsController>();
            //player.rb.velocity = Vector2.zero;
            //player.ApplyForce((player.transform.position - transform.position) * bounceForce);
            //Debug.Log("Boing");
            Explode();
        }

        //currentMode = EnemyMode.Moving;

        //movementDirecton.x = movementDirecton.x * -1;
    }




}

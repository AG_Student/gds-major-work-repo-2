﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack2 : MonoBehaviour
{
    public GameObject enemy;
    public GameObject cube;
    public GameObject player;
    public GameObject parent;
    public bool damage;
    public bool hit;

    // Start is called before the first frame update
    void Start()
    {
        damage = false;
    }

    // Update is called once per frame
    void Update()
    {
       if(this.GetComponent<MeshRenderer>().enabled == true && enemy.GetComponent<flyingEnemy>().Freeze == false)
       {
            enemy.GetComponent<flyingEnemy>().freeze();
            StartCoroutine(unstab1(2f));
       }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if(damage !=true)
            damage = true;
            if(damage ==true)
            stab(collision);
        }
    }

    public void stab(Collider2D collision)
    {
        enemy.GetComponent<flyingEnemy>().freeze();
        collision.gameObject.GetComponent<PlayerController>().Damage(1);
        //PlayerPhysicsController player = collision.gameObject.GetComponent<PlayerPhysicsController>();
        //player.rb.velocity = Vector2.zero;
        //player.ApplyForce((player.transform.position - transform.position) * -10);
        StartCoroutine(unstab(2f));
    }

    private IEnumerator unstab(float time)
    {
        yield return new WaitForSeconds(time);
        damage = false;
        this.GetComponent<MeshRenderer>().enabled = false;
        this.GetComponent<BoxCollider2D>().enabled = false;
        enemy.GetComponent<flyingEnemy>().Freeze = false;
        parent.GetComponentInParent<BoxCollider2D>().enabled = true;
    }

    private IEnumerator unstab1(float time)
    {
        yield return new WaitForSeconds(time);
        //damage = false;
        this.GetComponent<MeshRenderer>().enabled = false;
        this.GetComponent<BoxCollider2D>().enabled = false;
        enemy.GetComponent<flyingEnemy>().Freeze = false;
        parent.GetComponentInParent<BoxCollider2D>().enabled = true;
    }
}

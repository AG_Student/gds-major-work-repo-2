﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemy : MonoBehaviour, I_Damageable, I_Enemy
{

    public GameObject player;
    public GameObject shot, shotOrigin;
    public Vector2 moveVector;
    public float moveSpeed, attackRange, attackTimer;
    public int health;
    public Rigidbody2D rb;
    float attackTimerMax;
    public bool canMove = false, canShoot = false;
    public GameObject healthpickup;
    public int rapidFireCount = 0;
    public bool canDamage = true;

    public bool canSpawn = true;
    public SpriteRenderer spriteRenderer;
    public GameObject particles, splatter, light;

    WaveManager manager;
    bool arenaEnemy = false;

    public float playerHeight;

    //private bool atEdge;
    //public Transform edgeCheck;
    //public float edgeCheckRadius;
    //public LayerMask whatIsEdge;



    // Start is called before the first frame update
    public void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        player = ComboManager.Instance.player;
        playerHeight = player.GetComponent<SpriteRenderer>().bounds.size.y;
        rb = GetComponent<Rigidbody2D>();
        attackTimerMax = attackTimer;

    }

    public void AddToWave(WaveManager _manager)
    {
        manager = _manager;
        arenaEnemy = true;
    }

    // Update is called once per frame
    void Update()
    {
        GetTarget();

        //atEdge = Physics2D.OverlapCircle(edgeCheck.position, edgeCheckRadius, whatIsEdge);
    }
    void GetTarget()
    {
        moveVector = player.transform.position - transform.position;
        moveVector.y = 0;

        if (player.transform.position.x > this.transform.position.x)
        {
            this.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }
    private void FixedUpdate()
    {
        if (CheckAttackDistance() == true)
        {
            if (canShoot)
                Attack();
        }

        if (canMove)
            Move(moveVector);

    }
    void Move(Vector2 dir)
    {
        dir = dir.normalized;
        dir = dir * moveSpeed;
        rb.velocity = new Vector2(dir.x, rb.velocity.y);
        //if (!atEdge)
        //{
            //moveSpeed = 0;
        //}
    }
    bool CheckAttackDistance()
    {
        float distance = Vector2.Distance(player.transform.position, transform.position);

        if (distance <= attackRange)
        {
            return true;
        }
        else
            return false;
    }

    void singleAttack()
    {
        GameObject bullet = Instantiate(shot, shotOrigin.transform.position, Quaternion.identity);
        bullet.GetComponent<MachineGunBullet>().SetTrajectory(player.transform.position + (new Vector3(0, playerHeight)/2) - shotOrigin.transform.position);
        Debug.Log(player.transform.position);
        bullet.GetComponent<MachineGunBullet>().Rotate(-(player.transform.position - transform.position));
        attackTimer = attackTimerMax;
    }

    IEnumerator rapidFire()
    {
        int random = Random.Range(3, 6);
        attackTimerMax = 0.1f;
        while (random > 0)
        {
            singleAttack();
            random--;
            Debug.Log("fire fire fire");
            yield return new WaitForSeconds(attackTimerMax);
        }
        attackTimerMax = 2f;

    }

    void Attack()
    {
        if (attackTimer <= 0f)
        {
            if (rapidFireCount < 5)
            {
                GameObject bullet = Instantiate(shot, shotOrigin.transform.position, Quaternion.identity);
                bullet.GetComponent<MachineGunBullet>().SetTrajectory(player.transform.position + (new Vector3(0, playerHeight)/2) - shotOrigin.transform.position);
                bullet.GetComponent<MachineGunBullet>().Rotate(-(player.transform.position - transform.position));
                attackTimer = attackTimerMax;
                rapidFireCount++;
            }
            else if (rapidFireCount == 5)
            {
                Debug.Log("rapid 5");
                StartCoroutine(rapidFire());
                rapidFireCount = 0;
            }
        }
        else
        {
            attackTimer -= Time.deltaTime;
        }
    }
    public void Damage(int damage)
    {
        int chance = Random.Range(0, 101);

        int oddsArray = 0; 
        if (canDamage)
        {
            health -= damage;
            StartCoroutine(HitInvuln());
            if (health <= 0f)
            {

                AudioManager.Instance.PlayEnemyExplosion();

                if (GamePlayManager.Instance.currentPlayerHealth == GamePlayManager.Instance.maxHealth)
                {
                    oddsArray = 0;
                    if (chance <= GamePlayManager.Instance.oddsOn[oddsArray] && canSpawn == true)
                    {
                        //playVoiceLine();
                        SpawnHealth();
                        DestroyEnemy();
                    }
                    else
                    {
                        //playVoiceLine();
                        DestroyEnemy();
                    }

                }

                else if (GamePlayManager.Instance.currentPlayerHealth == GamePlayManager.Instance.halfHealth)
                {
                    oddsArray = 1;
                    if (chance <= GamePlayManager.Instance.oddsOn[oddsArray] && canSpawn == true)
                    {
                        SpawnHealth();
                        DestroyEnemy();

                    }
                    else
                    {
                        DestroyEnemy();
                    }
                }

                else if (GamePlayManager.Instance.currentPlayerHealth == GamePlayManager.Instance.quaterHealth)
                {
                    oddsArray = 2;
                    if (chance <= GamePlayManager.Instance.oddsOn[oddsArray] && canSpawn == true)
                    {
                        SpawnHealth();
                        DestroyEnemy();
                    }
                    else
                    {
                        DestroyEnemy();
                    }
                }
                //DestroyEnemy();
            }
        }
    }
    
    //public void playVoiceLine()
    //{
        //int i = Random.Range(0, 100);
        //Debug.Log(i);
        //if (i >= 50)
        //{
            //AudioManager.Instance.randomVoiceLine();
        //}
    //}

    public void SpawnHealth()
    {

        Instantiate(healthpickup, new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + .25f, this.gameObject.transform.position.z), Quaternion.identity);
        canSpawn = false;

    }

    public void DestroyEnemy()
    {
        Vector3 position = this.transform.position;
      //  DisplayScore.Instance.displayPoints(30, position);
        ComboManager.Instance.CallCombo(30f);
        player.GetComponent<PlayerController>().DodgeReset();
        Instantiate(particles, transform.position, Quaternion.identity);
        Instantiate(splatter, transform.position, Quaternion.identity);
        Instantiate(light, transform.position, Quaternion.identity);
        StartCoroutine(HitStop());
    }

    public void Stun()
    {
        canMove = false;
        rb.velocity = Vector3.zero;
    }

    public IEnumerator HitInvuln()
    {
        
        float invulnWindow = .25f;
        canDamage = false;
        canMove = false;
        canShoot = false; 
        spriteRenderer.color = Color.red;

        while (invulnWindow > 0)
        {
            invulnWindow -= Time.deltaTime;
            yield return null;
        }
        canDamage = true;
        canMove = true;
        canShoot = true;
        spriteRenderer.color = Color.white;
    }

    public void Knockback(Vector2 force)
    {
        rb.AddForce(force);
    }

    public void RemoveFromWave()
    {
        // if(manager.currentEnemies.Contains(this.gameObject))
        manager.enemyCount -= 1;
        Debug.LogWarning(manager.enemyCount);
    }

    void OnDestroy()
    {

    }

    public IEnumerator HitStop()
    {
        SlowTimeController controller = player.gameObject.GetComponent<SlowTimeController>();
        if (!controller.slowingTime)
        {
            float i = .1f;
            float oldScale = Time.timeScale;
            Time.timeScale = 0f;
             
            while (i > 0f)
            {
                //if (Time.timeScale != oldScale && Time.timeScale != 0f)
                //    Time.timeScale = 1f;
                i -= Time.unscaledDeltaTime;
                yield return null;
            }
            Time.timeScale = 1f;
            Time.fixedDeltaTime = Time.timeScale * .02f;
        }
        ScreenShakeController.Instance.ShakeCamera(2);
        if (arenaEnemy)
            RemoveFromWave();
        Destroy(this.gameObject);
        yield return null;
    }

 

    }

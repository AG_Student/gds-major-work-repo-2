﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBounce : MonoBehaviour
{
    I_Damageable enemy;
    public float bounceForce;

    private void Start()
    {
        enemy = GetComponent<I_Damageable>();
        bounceForce = GamePlayManager.Instance.bounceHeight;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player" && collision.gameObject.layer != LayerMask.NameToLayer("Detector") && collision.gameObject.GetComponent<PlayerController>().OnGround != true)
        {
            PlayerPhysicsController player = collision.gameObject.GetComponent<PlayerPhysicsController>();
            player.rb.velocity = Vector2.zero;
            player.ApplyForce((player.transform.position - transform.position) * bounceForce);
            player.player.RefillAmmo();
            enemy.Damage(10);
        }
    }

}

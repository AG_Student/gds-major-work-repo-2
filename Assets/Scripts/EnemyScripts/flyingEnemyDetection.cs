﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flyingEnemyDetection : MonoBehaviour
{
    [SerializeField]
    private GameObject parent;


    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            parent.GetComponent<flyingEnemy>().canMove = true;
            //parent.GetComponent<flyingEnemy>().canShoot = true;
            parent.GetComponent<flyingEnemy>().moveChange = true;
            parent.GetComponent<flyingEnemy>().fastChange();
        }
    }
}

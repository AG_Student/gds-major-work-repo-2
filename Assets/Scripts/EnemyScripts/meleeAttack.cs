﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meleeAttack : MonoBehaviour
{
    public GameObject enemy;
    public GameObject cube;
    public GameObject player;
    public GameObject child;
    // Start is called before the first frame update
    void Start()
    { 
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            hit(collision);
        }
    }

    public void hit(Collider2D collision)
    {
        this.GetComponent<MeshRenderer>().enabled = true;
        StartCoroutine(unhit(0.4f));
    }

    private IEnumerator unhit(float time)
    {
        yield return new WaitForSeconds(time);
        this.GetComponent<MeshRenderer>().enabled = false;
        this.GetComponent<BoxCollider2D>().enabled = false;
        child.GetComponentInChildren<MeshRenderer>().enabled = true;
        child.GetComponentInChildren<BoxCollider2D>().enabled = true;
    }

}

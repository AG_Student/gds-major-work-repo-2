﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetection : MonoBehaviour
{
    [SerializeField]
    private GameObject parent;

 
    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            parent.GetComponent<BasicEnemy>().canMove = true;
            parent.GetComponent<BasicEnemy>().canShoot = true;
        }
    }
}

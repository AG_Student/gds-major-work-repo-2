﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingEnemyBounce : MonoBehaviour
{
    private ExplodingEnemy parent;
    public float bounceForce;


    // Start is called before the first frame update
    void Start()
    {
        parent = gameObject.GetComponentInParent<ExplodingEnemy>();
        bounceForce = GamePlayManager.Instance.bounceHeight;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            PlayerPhysicsController player = collision.gameObject.GetComponent<PlayerPhysicsController>();
            player.rb.velocity = Vector2.zero;
            player.ApplyForce((player.transform.position - transform.position) * bounceForce);
            Debug.Log("Boing");
            parent.Explode();
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    //basic script for infinitely spawning enemies

    public Transform[] spawnPoints;

    public GameObject spawnObj;
    //public GameObject rangedEnemy, enemy;
    //public int spawnCount, spawnTimer; 

    private void Start()
    {
        spawnPoints[0] = this.transform;
    }

    public void SpawnEnemy(GameObject enemy)
    {
        //int spawnPoint = Random.Range(0, spawnPoints.Length);
        //int enemyType = Random.Range(0, 2);
        //if(enemyType == 0)
        //{
        //    GameObject spawnedEnemy = Instantiate(rangedEnemy, spawnPoints[spawnPoint].position, spawnPoints[spawnPoint].rotation);

        //}
        //else if (enemyType == 1)
        //{
        //    GameObject spawnedEnemy = Instantiate(enemy, spawnPoints[spawnPoint].position, spawnPoints[spawnPoint].rotation);
        //}

        //spawnCount--;
        //if (spawnCount > 0f)
        //{
        //    Invoke("SpawnEnemy", spawnTimer);
        //}

        Instantiate(enemy, spawnPoints[0].position, Quaternion.identity);

    }

    public void SpawnWithReference(GameObject enemy, WaveManager manager)
    {
        //GameObject spawnEnemy = Instantiate(enemy, spawnPoints[0].position, Quaternion.identity);
        //spawnEnemy.GetComponent<I_Enemy>().AddToWave(manager);
        StartCoroutine(ScaleUp(enemy, manager));
    }

    void SpawnSubFunc(GameObject enemy, WaveManager manager)
    {
        GameObject spawnEnemy = Instantiate(enemy, spawnPoints[0].position, Quaternion.identity);
        spawnEnemy.GetComponent<I_Enemy>().AddToWave(manager);
    }

    IEnumerator ScaleUp(GameObject objToScale, WaveManager manager)
    {
        GameObject _spawnObj = Instantiate(spawnObj, spawnPoints[0].position, Quaternion.identity);
        _spawnObj.transform.localScale = Vector3.zero;
        while (_spawnObj.transform.localScale.x < 1f)
        {
            _spawnObj.transform.localScale += new Vector3(Time.deltaTime,Time.deltaTime,Time.deltaTime) * 3;
            yield return null;
        }
        _spawnObj.transform.localScale = new Vector3(1f, 1f, 1f);
        Destroy(_spawnObj);
        SpawnSubFunc(objToScale, manager);
    }

}

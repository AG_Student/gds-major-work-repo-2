﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLedgeCheck : MonoBehaviour
{
    private bool atEdge;
    public Transform edgeCheck;
    public float edgeCheckradius;
    public LayerMask edgeMask;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        atEdge = Physics2D.OverlapCircle(edgeCheck.position, edgeCheckradius, edgeMask);

        if (atEdge)
        {

            if (transform.rotation.y == 180)
            {
                transform.rotation = new Quaternion(0, 0, 0, 0);
                          
            }
            else
            {
                transform.rotation = new Quaternion(0, 180, 0, 0);
            }
        }
    }
}

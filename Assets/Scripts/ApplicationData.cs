﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ApplicationData
{
    public static bool canChange = true;
    public static bool canChainsaw = true;

    public static bool tutorial = true;

    public static float score = 0f;
    public static float lastScore = 0f;

    public enum CurrentLevel 
    { 
        level1,
        level2,
        levelEndless
    }

    public static CurrentLevel currentLevel;


    public static Vector3 spawnLocation = new Vector3(-203.87f, -6.1f, 0);
    public static Vector3 originalSpawn = new Vector3(-203.87f, -6.1f, 0);
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalculateGrade : MonoBehaviour
{
    Dictionary<float, string> grades;
    public Text text1, text2;
    float score;

    // Start is called before the first frame update
    void Start()
    {
        grades = new Dictionary<float, string>();
        grades.Add(1000,"D");
        grades.Add(1500,"C");
        grades.Add(2000,"B");
        grades.Add(2500,"A");
        grades.Add(3000,"S");
        grades.Add(3500,"Hectic");
        GetGrade();
    }

    void GetGrade()
    {
        RoundScore();

        text1.text = "Final Score: " +  ApplicationData.score.ToString();
        text2.text = "Final Grade: " + grades[ score];
    }

    void RoundScore()
    {
          score = ApplicationData.score;
        if (score < 1000)
            score = 1000;
        else if (score < 1500)
            score = 1500;
        else if (score < 2000)
            score = 2000;
        else if (score < 2500)
            score = 2500;
        else if (score < 3000)
            score = 3000;
        else if (score < 3500)
            score = 3500;
        else if (score > 3500)
            score = 3500;


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainsawScript : MonoBehaviour, I_Weapon
{
    PlayerPhysicsController player;
    PlayerController playerCon;
    public Vector3 dir;
    public float force;
    float tempScale;
    public bool active = false;
    public GameObject particles;
    GameObject enemy;


    public float fuel = 5f;
    float maxFuel;

    public FuelGauge fuelGauge;

    int maxAmmo = 30, currentAmmo;
    float tempDrag;
    bool refuel, currentlyUsing = false;



    // Start is called before the first frame update
    void Start()
    {
        player = GetComponentInParent<PlayerPhysicsController>();
        playerCon = GetComponentInParent<PlayerController>();
        currentAmmo = maxAmmo;
        maxFuel = fuel;
        fuelGauge.maxHP = maxFuel;
        fuelGauge.currentHP = fuel;
        refuel = false;
        AudioManager.Instance.SetChainsawClip(1);
    }

    // Update is called once per frame
    public void Attack()
    {
        //intentionally blank
    }

    private void Update()
    {
        if (!active)
        {
            playerCon.gameObject.GetComponent<WeaponManager>().EnableWeapon(0);
        }
    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (active)
        {

            AudioManager.Instance.SetChainsawClip(0);

            if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {

                force = 3f;
                playerCon.canMove = false;
                dir = new Vector3(player.rb.velocity.x, 0, 0);
                if (dir.x == 0f)
                    dir = new Vector3(1, 0, 0);
                dir = dir.normalized;
                tempDrag = player.rb.drag;
                player.rb.drag = 0;
                player.ApplyForce(dir * force);

                currentlyUsing = true;
                ScreenShakeController.Instance.ShakeCamera(.5f);

            }
            else if (collision.gameObject.tag == "Wall")
            {
                force = 5f;
                playerCon.canMove = false;
               // Debug.Log((collision.GetContact(0).point) - (Vector2)transform.position);
                if (((collision.GetContact(0).point) - (Vector2)transform.position).x > 0f)
                    dir = new Vector3(1, 1, 0);
                else if (((collision.GetContact(0).point) - (Vector2)transform.position).x < 0f)
                    dir = new Vector3(-1, 1, 0);
                tempScale = player.rb.gravityScale;
                player.rb.gravityScale = 0f;
                player.rb.velocity = dir * force; 
                ScreenShakeController.Instance.ShakeCamera(.5f);
            }

            else if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                enemy = collision.gameObject;
                StartCoroutine(ShredEnemy());
                fuel -= 1;
                fuelGauge.fuel -= 1;
                player.ApplyForce(Vector2.up * 3f); 
            }

        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {

        AudioManager.Instance.SetChainsawClip(1);

        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            playerCon.canMove = true;
            dir = Vector3.zero;
            player.rb.drag = tempDrag; 

        }
        else if (collision.gameObject.tag == "Wall")
        {
            playerCon.canMove = true;
            player.rb.gravityScale = tempScale;
            dir = Vector3.zero;
             
        }
    }

    IEnumerator ShredEnemy()
    {
         
        ScreenShakeController.Instance.ShakeCamera(2);
        float i = .1f; 
        //Time.timeScale = 0f;
        Instantiate(particles, transform.position, Quaternion.identity);
        //while (i > 0f)
        //{
        //    i -= Time.unscaledDeltaTime;
        //    yield return null;
        //}
        i = .1f;
        Instantiate(particles, transform.position, Quaternion.identity);
        //while (i > 0f)
        //{
        //    i -= Time.unscaledDeltaTime;
        //    yield return null;
        //}
        i = .1f;
        Instantiate(particles, transform.position, Quaternion.identity);
        // Time.timeScale = 1f;
        yield return new WaitForSecondsRealtime(.2f); 
        enemy.GetComponent<I_Damageable>().Damage(5);
        yield return null;

    }
    public int ReturnAmmo()
    {
        return maxAmmo;
    }

    public int ReturnCurrentAmmo()
    {
        return currentAmmo;
    }

    public int ReturnMaxAmmo()
    {
        return maxAmmo;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {

        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground") || (collision.gameObject.tag == "Wall") || collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            if(active)
            fuelGauge.fuel -= Time.deltaTime*2;
        }

    }
}

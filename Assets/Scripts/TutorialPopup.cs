﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPopup : MonoBehaviour
{
    public List<GameObject> popups;
    public GameObject popup;
    int popupCount;
    bool active = false;
    Collider2D collision;
    public float timer2 = 5;

    private void Start()
    {
        popupCount = 0;
        popup = popups[popupCount];
    }

    private void Update()
    {
       // timer2 -= Time.deltaTime;

        if (active)
            if ( popupCount < popups.Count - 1 && Input.GetKeyDown(KeyCode.Space))
                NextWindow();
            else if (popupCount >= popups.Count - 1 && Input.GetKeyDown(KeyCode.Space))
                ClosePopup();
        //if (timer2 <= 0f)
        //    timer2 = 5;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && ApplicationData.tutorial)
        {
            Popup();
           // active = true;
            collision.gameObject.GetComponent<SlowTimeController>().canUse = false;
            //collision.gameObject.GetComponent<weaponWheel>().canuse = false;
            collision.gameObject.GetComponent<PlayerController>().canUse = false;
            this.collision = collision;
        }

    }

    private void Popup()
    {
        popup.SetActive(true);
         Time.timeScale = 0f;
        StartCoroutine(Timer());
    }

    void NextWindow()
    {
        popupCount++;
        Destroy(popup);
        active = false;
        timer2 = 5;
        popup = popups[popupCount];
        popup.SetActive(true);
        StartCoroutine(Timer());
    }

    void ClosePopup()
    {
        Destroy(popup);
        timer2 = 5;
        Time.timeScale = 1f;
        collision.gameObject.GetComponent<SlowTimeController>().canUse = true;
        //collision.gameObject.GetComponent<weaponWheel>().canuse = true; 
        collision.gameObject.GetComponent<PlayerController>().canUse = true;
        Destroy(this.gameObject);
    }

    IEnumerator Timer()
    {
        float i = .5f;
        while (i > 0)
        {
            i -= Time.unscaledDeltaTime;
            yield return null;
        }
        active = true;
    }
}

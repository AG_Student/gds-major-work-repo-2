﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public List<Wave> waves;
    public List<GameObject> currentEnemies;
    int currentWave = 0; 

    public List<spawner> spawners;

    public GameObject[] gates;

    bool canDestroy = false;

    public int enemyCount;

    private void Awake()
    {
        LoadNextWave();
        StartCoroutine(SpawnWave(currentWave));
    }

    private void Update()
    {
        CheckWave(); 
    }

    IEnumerator SpawnWave(int wave)
    {
        enemyCount = waves[wave].enemies.Count;
        int spawnerSelect = 0;
        foreach(GameObject enemy in waves[wave].enemies)
        {
            spawners[spawnerSelect].SpawnWithReference(enemy, this);
            yield return new WaitForSeconds(.5f);
            if (spawnerSelect < spawners.Count - 1)
                spawnerSelect++;
            else if (spawnerSelect >= spawners.Count - 1)
                spawnerSelect = 0;
            currentEnemies.Add(enemy);
        }
        canDestroy = true;
    }

    

    void CheckWave()
    {
        if (enemyCount <= 0 && canDestroy)
        {
            if (currentWave < waves.Count - 1)
            {
                canDestroy = false;
                currentWave++;
                LoadNextWave();
                StartCoroutine(SpawnWave(currentWave));

            }
            else if (canDestroy)
            {
                StopSpawning();
            }
        }
    }

    void LoadNextWave()
    {
        currentEnemies = new List<GameObject>();
        //foreach (GameObject enemy in waves[currentWave].enemies)
        //{
        //    currentEnemies.Add(enemy);
        //}

    }

    void StopSpawning()
    {
        //Open Gates
        if (gates != null)
            OpenGates();

        Destroy(this.gameObject);
    }

    void OpenGates()
    {
        foreach(GameObject gate in gates)
        {
            gate.GetComponent<ArenaDoor>().UnlockDoor();
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupFollow : MonoBehaviour
{
    public float moveSpeed = 3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("hey");
            transform.Translate(collision.gameObject.transform.position * moveSpeed * Time.deltaTime);
        }
    }
}

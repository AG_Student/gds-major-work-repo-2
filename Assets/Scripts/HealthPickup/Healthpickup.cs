﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healthpickup : MonoBehaviour
{
    // takes the healthbar
    public GameObject healthbar;
    public GameObject particles;

    // Start is called before the first frame update
    //private IEnumerator CountDown()
    //{
       

    //}
    
    void Start()
    {
        Destroy(this.transform.parent.gameObject, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            if (collision.gameObject.GetComponent<PlayerController>().bar.currentHP < collision.gameObject.GetComponent<PlayerController>().bar.maxHP)
            {
                AudioManager.Instance.PlayPickup();
                collision.gameObject.GetComponent<PlayerController>().bar.currentHP += 1;
                Instantiate(particles, transform.position, Quaternion.identity);
                Destroy(this.transform.parent.gameObject);
            }
        }
    }
}

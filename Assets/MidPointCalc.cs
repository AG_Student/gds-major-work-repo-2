﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidPointCalc : MonoBehaviour
{
    public GameObject g1, g2;

    Vector3 midpoint;
 

    // Update is called once per frame
    void Update()
    {
        midpoint = g1.transform.position - (g1.transform.position  - g2.transform.position) / 2;
        transform.position = new Vector2(midpoint.x, midpoint.y);
    }
}

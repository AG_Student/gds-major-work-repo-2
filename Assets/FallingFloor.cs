﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingFloor : MonoBehaviour
{
    public float fallTime = 2f;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
            StartCoroutine(Collapse());
    }

    IEnumerator Collapse()
    {
        GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(fallTime);
        GetComponent<Rigidbody2D>().isKinematic = false;
        GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(3f);
        Destroy(this.gameObject);
    }
}

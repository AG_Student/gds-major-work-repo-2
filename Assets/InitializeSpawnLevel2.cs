﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializeSpawnLevel2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        if(ApplicationData.spawnLocation == ApplicationData.originalSpawn)
        ApplicationData.spawnLocation = new Vector3(-72.5f, 5.05f, 0f);

        ApplicationData.canChainsaw = true;
        ApplicationData.canChange = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

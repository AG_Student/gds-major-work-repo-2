﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : KillPit
{
    private new void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.tag != "Chainsaw")
            base.OnCollisionEnter2D(collision);
    }
}

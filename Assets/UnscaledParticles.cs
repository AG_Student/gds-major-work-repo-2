﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnscaledParticles : MonoBehaviour
{

    ParticleSystem particleSystem;

    // Start is called before the first frame update
    void Start()
    {

        particleSystem = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale < .01f)
            particleSystem.Simulate(Time.unscaledDeltaTime, true, false); 
    }
}

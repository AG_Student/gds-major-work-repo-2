﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class AddToLightManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GamePlayManager.Instance.lights.Add(this.gameObject.GetComponent<Light2D>());
    }
     
}
